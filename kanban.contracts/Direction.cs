﻿namespace kanban.contracts
{
	public enum Direction : byte
	{
		Up,
		Down,
		Left,
		Right
	}
}
