﻿using System.Collections;
using System.Collections.Generic;

namespace kanban.contracts
{
    public interface IBoardConfigProvider
    {
        IEnumerable<string> ReadConfig();
    }
}