﻿using System.Collections;
using System.Collections.Generic;

namespace kanban.contracts
{
    public interface ITaskFileProvider
    {
        IEnumerable<string> ReadTaskFile();

        void WriteTaskFile(IEnumerable<string> lines);
    }
}