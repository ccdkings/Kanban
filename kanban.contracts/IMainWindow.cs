﻿using System;
using System.Collections.Generic;

namespace kanban.contracts
{
	public delegate void OnMoveAction(Direction direction, out IList<IList<Task>> columnsOfTasks, out IList<bool> wipLimitExceeded);

    public interface IMainWindow
    {
        void Init(IEnumerable<ColumnHeader> columnHeaders, IList<IList<Task>> columnsOfTasks, IList<bool> wipLimitExceeded);
        Func<string, IList<IList<Task>>> OnNew { get; set; }
        Func<Guid, IList<IList<Task>>> OnSelect { get; set; }
		Func<string, IList<IList<Task>>> OnModify { get; set; }
	}
}