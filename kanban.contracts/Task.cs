﻿using System;

namespace kanban.contracts
{
    public class Task
    {
        public string Text { get; set; }
        public Guid Id { get; set; }
        public bool Selected { get; set; }
    }
}