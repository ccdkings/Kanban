﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace kanban.contracts
{
    public interface IKanban
    {
        IEnumerable<ColumnHeader> CreateColumnHeaders(IEnumerable<string> configLines);
        IList<IList<Task>> CreateTasks(IEnumerable<string> taskLines);
        Task CreateTask(string text);
        IList<IList<Task>> AppendTask(Task task, IList<IList<Task>> columnsOfTasks);
        IEnumerable<string> SerializeTasks(IList<IList<Task>> columnsOfTasks);
        void DeselectAll(IList<IList<Task>> columnsOfTasks);
        Task FindTaskById(Guid selected, IList<IList<Task>> columnsOfTasks);
        void SelectTask(Task task);

        void SetText(Task task, string text);
        Task FindSelectedTask(IList<IList<Task>> columnsOfTasks);
        Tuple<int, int> GetSourceCoordinate(Task task, IList<IList<Task>> columnsOfTasks);
        Tuple<int, int> CalculateDestinationCoordinate(Tuple<int, int> source, Direction direction);

        IList<IList<Task>> MoveTask(IList<IList<Task>> columnsOfTasks, Tuple<int, int> source,
            Tuple<int, int> destination);
    }
}