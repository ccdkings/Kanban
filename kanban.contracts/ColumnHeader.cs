﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kanban.contracts
{
    public class ColumnHeader
    {
        public string Name { get; set; }
        public int WipLimit { get; set; } // WipLimit == 0 -> unlimited
    }
}
