﻿using System;
using System.Collections.Generic;
using kanban.contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace kanban.ui.test
{
	[TestClass]
	public class ColumnVMTests
	{
		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void ColumnVM_Construct_ColumnHeaderNull()
		{
			//Arange
			ColumnHeader columnHeader = null;
			var tasks = CreateTestTasks(3);

			//Act
			var vm = new ColumnVM(columnHeader, tasks, false, MockOnSelect);

			//Assert
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void ColumnVM_Construct_TasksNull()
		{
			//Arange
			var columnHeader = CreateTestColumnHeader("Column1", 0);
			IEnumerable<Task> tasks = null;

			//Act
			var vm = new ColumnVM(columnHeader, tasks, false, MockOnSelect);

			//Assert
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void ColumnVM_Construct_OnSelectNull()
		{
			//Arange
			var columnHeader = CreateTestColumnHeader("Column1", 0);
			var tasks = CreateTestTasks(3);

			//Act
			var vm = new ColumnVM(columnHeader, tasks, false, null);

			//Assert
		}

		[TestMethod]
		public void ColumnVM_Title_NoWipLimit()
		{
			//Arange
			string columnName = "Column1";
			int wipLimit = 0;
			string expectedTitle = "Column1";
			var columnHeader = CreateTestColumnHeader(columnName, wipLimit);
			var tasks = CreateTestTasks(3);
			var vm = new ColumnVM(columnHeader, tasks, false, MockOnSelect);

			//Act
			string title = vm.Title;

			//Assert
			Assert.AreEqual(expectedTitle, title);
		}

		[TestMethod]
		public void ColumnVM_Title_WithWipLimit()
		{
			//Arange
			string columnName = "Column1";
			int wipLimit = 3;
			string expectedTitle = "Column1 (3)";
			var columnHeader = CreateTestColumnHeader(columnName, wipLimit);
			var tasks = CreateTestTasks(3);
			var vm = new ColumnVM(columnHeader, tasks, false, MockOnSelect);

			//Act
			string title = vm.Title;

			//Assert
			Assert.AreEqual(expectedTitle, title);
		}

		[TestMethod]
		public void ColumnVM_Tasks_NoTasks()
		{
			//Arange
			string columnName = "Column1";
			int wipLimit = 0;
			int taskCount = 0;
			var columnHeader = CreateTestColumnHeader(columnName, wipLimit);
			var tasks = CreateTestTasks(taskCount);
			var vm = new ColumnVM(columnHeader, tasks, false, MockOnSelect);

			//Act
			var taskCollection = vm.Tasks;

			//Assert
			Assert.IsNotNull(taskCollection);
			Assert.AreEqual(taskCount, taskCollection.Count);
		}

		[TestMethod]
		public void ColumnVM_Tasks_WithTasks()
		{
			//Arange
			string columnName = "Column1";
			int wipLimit = 0;
			int taskCount = 3;
			var columnHeader = CreateTestColumnHeader(columnName, wipLimit);
			var tasks = CreateTestTasks(taskCount);
			var vm = new ColumnVM(columnHeader, tasks, false, MockOnSelect);

			//Act
			var taskCollection = vm.Tasks;

			//Assert
			Assert.IsNotNull(taskCollection);
			Assert.AreEqual(taskCount, taskCollection.Count);
		}

		[TestMethod]
		public void ColumnVM_Tasks_RefreshTasks()
		{
			//Arange
			string columnName = "Column1";
			int wipLimit = 0;
			var columnHeader = CreateTestColumnHeader(columnName, wipLimit);
			int taskCount = 3;
			var tasks = CreateTestTasks(taskCount);
			var vm = new ColumnVM(columnHeader, tasks, false, MockOnSelect);
			int newTaskCount = 4;
			var newTasks = CreateTestTasks(newTaskCount);
			vm.RefreshTasks(newTasks);

			//Act
			var taskCollection = vm.Tasks;

			//Assert
			Assert.IsNotNull(taskCollection);
			Assert.AreEqual(newTaskCount, taskCollection.Count);
		}

		private void MockOnSelect(Guid id)
		{ }

		private ColumnHeader CreateTestColumnHeader(string name, int wipLimit)
		{
			return new ColumnHeader() { Name = name, WipLimit = wipLimit };
		}

		private IList<Task> CreateTestTasks(int numTasks)
		{
			var result = new List<Task>();
			for (int cnt = 0; cnt < numTasks; cnt++)
			{
				result.Add(new Task() { Id = Guid.NewGuid(), Text = "Task" + (cnt + 1) });
			}
			return result;
		}
	}
}
