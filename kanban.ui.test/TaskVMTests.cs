﻿using System;
using System.Windows.Input;
using kanban.contracts;
using kanban.ui.wpf;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace kanban.ui.test
{
	[TestClass]
	public class TaskVMTests
	{
		Guid mockId;
		int mockOnSelectCall = 0;

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void TaskVM_Construct_TaskNull()
		{
			//Arange
			Task task = null;

			//Act
			var vm = new TaskVM(task, MockOnSelect);

			//Assert
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void TaskVM_Construct_OnSelectNull()
		{
			//Arange
			Guid id = Guid.NewGuid();
			string text = "TaskText";
			Task task = new Task() { Id = id, Text = text, Selected = false };

			//Act
			var vm = new TaskVM(task, null);

			//Assert
		}

		[TestMethod]
		public void TaskVM_Construct_OK()
		{
			//Arange
			Guid id = Guid.NewGuid();
			string text = "TaskText";
			bool selected = true;
			Task task = new Task() { Id = id, Text = text, Selected = selected };

			//Act
			var vm = new TaskVM(task, MockOnSelect);

			//Assert
			Assert.AreEqual(id, vm.Id);
			Assert.AreEqual(text, vm.Text);
			Assert.AreEqual(selected, vm.Selected);
			Assert.IsNotNull(vm.SelectCommand);
			Assert.IsInstanceOfType(vm.SelectCommand, typeof(RelayCommand));
		}

		[TestMethod]
		public void TaskVM_SelectCommand_OnSelectOK()
		{
			//Arange
			mockId = Guid.Empty;
			mockOnSelectCall = 0;

			Guid id = Guid.NewGuid();
			string text = "TaskText";
			bool selected = false;
			Task task = new Task() { Id = id, Text = text, Selected = selected };
			var vm = new TaskVM(task, MockOnSelect);
			ICommand cmd = vm.SelectCommand;

			//Act
			cmd.Execute(null);

			//Assert
			Assert.AreEqual(id, mockId);
			Assert.AreEqual(1, mockOnSelectCall);
			Assert.AreEqual(id, vm.Id);
			Assert.AreEqual(text, vm.Text);
			Assert.AreEqual(selected, vm.Selected);
		}

		private void MockOnSelect(Guid id)
		{
			mockId = id;
			mockOnSelectCall++;
		}
	}
}
