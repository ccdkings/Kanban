﻿using System;
using System.Collections.Generic;
using System.Linq;
using kanban.contracts;
using kanban.ui.wpf;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace kanban.ui.test
{
	[TestClass]
	public class MainWindowTests
	{
		[TestMethod]
		public void MainWindow_Construct()
		{
			//Arange

			//Act
			var mw = new MainWindow();

			//Assert
			Assert.IsNotNull(mw);
			Assert.IsNotNull(mw.NewCommand);
			Assert.IsInstanceOfType(mw.NewCommand, typeof(RelayCommand));
			Assert.IsNotNull(mw.ModifyCommand);
			Assert.IsInstanceOfType(mw.ModifyCommand, typeof(RelayCommand));
			Assert.IsNotNull(mw.DeleteCommand);
			Assert.IsInstanceOfType(mw.DeleteCommand, typeof(RelayCommand));
			Assert.IsNotNull(mw.UpCommand);
			Assert.IsInstanceOfType(mw.UpCommand, typeof(RelayCommand));
			Assert.IsNotNull(mw.LeftCommand);
			Assert.IsInstanceOfType(mw.LeftCommand, typeof(RelayCommand));
			Assert.IsNotNull(mw.RightCommand);
			Assert.IsInstanceOfType(mw.RightCommand, typeof(RelayCommand));
			Assert.IsNotNull(mw.DownCommand);
			Assert.IsInstanceOfType(mw.DownCommand, typeof(RelayCommand));
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void MainWindow_Init_ColumHeadersNull()
		{
			//Arange
			var mw = new MainWindow();
			IEnumerable<ColumnHeader> columns = null;
			var tasks = CreateTestTasks(3, 2);
			var wipLimitViolations = CreateTestWipLimitViolations(3);

			//Act
			mw.Init(columns, tasks, wipLimitViolations);

			//Assert
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void MainWindow_Init_TasksNull()
		{
			//Arange
			var mw = new MainWindow();
			var columns = CreateTestColumnHeaders(3);
			IList<IList<Task>> tasks = null;
			var wipLimitViolations = CreateTestWipLimitViolations(3);

			//Act
			mw.Init(columns, tasks, wipLimitViolations);

			//Assert
		}

		[TestMethod]
		[ExpectedException(typeof(ArgumentOutOfRangeException))]
		public void MainWindow_Init_HeadersAndTasksCountDifferent()
		{
			//Arange
			var mw = new MainWindow();
			int headerCnt = 3;
			int taskCnt = 4;
			var columns = CreateTestColumnHeaders(headerCnt);
			var tasks = CreateTestTasks(taskCnt, 2).ToList();
			var wipLimitViolations = CreateTestWipLimitViolations(headerCnt);

			//Act
			mw.Init(columns, tasks, wipLimitViolations);

			//Assert
		}

		[TestMethod]
		public void MainWindow_Init_NoColumns()
		{
			//Arange
			var mw = new MainWindow();
			int columnCnt = 0;
			var columns = CreateTestColumnHeaders(columnCnt);
			var tasks = CreateTestTasks(columnCnt, 2).ToList();
			var wipLimitViolations = CreateTestWipLimitViolations(columnCnt);

			//Act
			mw.Init(columns, tasks, wipLimitViolations);
			var columnCollection = mw.Columns;

			//Assert
			Assert.IsNotNull(columnCollection);
			Assert.AreEqual(columnCnt, columnCollection.Count);
		}

		[TestMethod]
		public void MainWindow_Init_WithColumns()
		{
			//Arange
			var mw = new MainWindow();
			int columnCnt = 3;
			var columns = CreateTestColumnHeaders(columnCnt);
			var tasks = CreateTestTasks(columnCnt, 2).ToList();
			var wipLimitViolations = CreateTestWipLimitViolations(columnCnt);

			//Act
			mw.Init(columns, tasks, wipLimitViolations);
			var columnCollection = mw.Columns;

			//Assert
			Assert.IsNotNull(columnCollection);
			Assert.AreEqual(columnCnt, columnCollection.Count);
		}

		[TestMethod]
		[ExpectedException(typeof(NullReferenceException))]
		public void MainWindow_NewCommand_OnNewNull()
		{
			//Arange
			var mw = new MainWindow();
			int columnCnt = 3;
			var columns = CreateTestColumnHeaders(columnCnt);
			var tasks = CreateTestTasks(columnCnt, 2).ToList();
			var wipLimitViolations = CreateTestWipLimitViolations(columnCnt);
			mw.Init(columns, tasks, wipLimitViolations);

			//Act
			mw.NewCommand.Execute(null);

			//Assert
		}

		[TestMethod]
		public void MainWindow_NewCommand_OnNewEmptyText()
		{
			//Arange
			var mw = new MainWindow();
			int columnCnt = 3;
			int taskCnt = 2;
			var columns = CreateTestColumnHeaders(columnCnt);
			var tasks = CreateTestTasks(columnCnt, taskCnt).ToList();
			var wipLimitViolations = CreateTestWipLimitViolations(columnCnt);
			mw.Init(columns, tasks, wipLimitViolations);
			string initText = "";
			mw.Text = initText;

			int newTaskCnt = 3;
			var newTasks = CreateTestTasks(columnCnt, newTaskCnt).ToList();
			string onNewText = "";
			mw.OnNew = (text) =>
			{
				onNewText = text;
				return newTasks;
			};

			//Act
			mw.NewCommand.Execute(null);

			//Assert
			Assert.AreEqual(initText, onNewText);
			Assert.AreEqual(newTaskCnt, mw.Columns[0].Tasks.Count);
		}

		[TestMethod]
		public void MainWindow_NewCommand_OnNewWithText()
		{
			//Arange
			var mw = new MainWindow();
			int columnCnt = 3;
			int taskCnt = 2;
			var columns = CreateTestColumnHeaders(columnCnt);
			var tasks = CreateTestTasks(columnCnt, taskCnt).ToList();
			var wipLimitViolations = CreateTestWipLimitViolations(columnCnt);
			mw.Init(columns, tasks, wipLimitViolations);
			string initText = "Hello World!";
			mw.Text = initText;

			int newTaskCnt = 3;
			var newTasks = CreateTestTasks(columnCnt, newTaskCnt).ToList();
			string onNewText = "";
			mw.OnNew = (text) =>
			{
				onNewText = text;
				return newTasks;
			};

			//Act
			mw.NewCommand.Execute(null);

			//Assert
			Assert.AreEqual(initText, onNewText);
			Assert.AreEqual(newTaskCnt, mw.Columns[0].Tasks.Count);
		}

		[TestMethod]
		[ExpectedException(typeof(NullReferenceException))]
		public void MainWindow_SelectCommand_OnSelectNull()
		{
			//Arange
			var mw = new MainWindow();
			int columnCnt = 3;
			var columns = CreateTestColumnHeaders(columnCnt);
			var tasks = CreateTestTasks(columnCnt, 2).ToList();
			var wipLimitViolations = CreateTestWipLimitViolations(columnCnt);
			mw.Init(columns, tasks, wipLimitViolations);
			var taskvm = mw.Columns[0].Tasks[0];

			//Act
			taskvm.SelectCommand.Execute(null);

			//Assert
		}

		[TestMethod]
		public void MainWindow_SelectCommand_OnSelectOK()
		{
			//Arange
			var mw = new MainWindow();
			int columnCnt = 3;
			int taskCnt = 2;
			var columns = CreateTestColumnHeaders(columnCnt);
			var tasks = CreateTestTasks(columnCnt, taskCnt).ToList();
			var wipLimitViolations = CreateTestWipLimitViolations(columnCnt);
			mw.Init(columns, tasks, wipLimitViolations);
			var taskvm = mw.Columns[0].Tasks[0];
			Guid id = taskvm.Id;
			string text = taskvm.Text;

			Guid idSelected = Guid.Empty;

			mw.OnSelect = (selectedId) =>
			{
				idSelected = selectedId;
				tasks[0][0].Selected = true;
				return tasks;
			};
			//Act
			taskvm.SelectCommand.Execute(null);

			//Assert
			Assert.AreEqual(id, idSelected);
			Assert.IsTrue(mw.Columns[0].Tasks[0].Selected);
			Assert.AreEqual(text, mw.Text);
		}

		[TestMethod]
		[ExpectedException(typeof(NullReferenceException))]
		public void MainWindow_ModifyCommand_OnModifyNull()
		{
			//Arange
			var mw = new MainWindow();
			int columnCnt = 3;
			var columns = CreateTestColumnHeaders(columnCnt);
			var tasks = CreateTestTasks(columnCnt, 2).ToList();
			var wipLimitViolations = CreateTestWipLimitViolations(columnCnt);
			mw.Init(columns, tasks, wipLimitViolations);

			//Act
			mw.ModifyCommand.Execute(null);

			//Assert
		}

		[TestMethod]
		public void MainWindow_ModifyCommand_OnModifyEmptyText()
		{
			//Arange
			var mw = new MainWindow();
			int columnCnt = 3;
			int taskCnt = 2;
			var columns = CreateTestColumnHeaders(columnCnt);
			var tasks = CreateTestTasks(columnCnt, taskCnt).ToList();
			var wipLimitViolations = CreateTestWipLimitViolations(columnCnt);
			tasks[1][1].Selected = true;
			mw.Init(columns, tasks, wipLimitViolations);
			string initText = "";
			mw.Text = initText;

			var modifyTasks = CreateTestTasks(columnCnt, taskCnt).ToList();
			modifyTasks[1][1].Selected = true;
			modifyTasks[1][1].Text = initText;

			string onModifyText = "";
			mw.OnModify = (text) =>
			{
				onModifyText = text;
				return modifyTasks;
			};

			//Act
			mw.ModifyCommand.Execute(null);

			//Assert
			Assert.AreEqual(initText, onModifyText);
		}

		[TestMethod]
		public void MainWindow_ModifyCommand_OnModifyWithText()
		{
			//Arange
			var mw = new MainWindow();
			int columnCnt = 3;
			int taskCnt = 2;
			var columns = CreateTestColumnHeaders(columnCnt);
			var tasks = CreateTestTasks(columnCnt, taskCnt).ToList();
			var wipLimitViolations = CreateTestWipLimitViolations(columnCnt);
			tasks[1][1].Selected = true;
			mw.Init(columns, tasks, wipLimitViolations);
			string initText = "Hello World!";
			mw.Text = initText;

			var modifyTasks = CreateTestTasks(columnCnt, taskCnt).ToList();
			modifyTasks[1][1].Selected = true;
			modifyTasks[1][1].Text = initText;

			string onModifyText = "";
			mw.OnModify = (text) =>
			{
				onModifyText = text;
				return modifyTasks;
			};

			//Act
			mw.ModifyCommand.Execute(null);

			//Assert
			Assert.AreEqual(initText, onModifyText);
		}

		private IEnumerable<ColumnHeader> CreateTestColumnHeaders(int numHeaders)
		{
			for (int cnt = 0; cnt < numHeaders; cnt++)
			{
				yield return new ColumnHeader() { Name = "Column" + (cnt + 1), WipLimit = 0 };
			}
		}

		private IList<IList<Task>> CreateTestTasks(int numHeaders, int numTasks)
		{
		    var result = new List<IList<Task>>();
			for (int cnt = 0; cnt < numHeaders; cnt++)
			{
                result.Add(CreateTestTasks(numTasks, "Col" + (cnt + 1)));
			}
		    return result;
		}

		private IList<Task> CreateTestTasks(int numTasks, string suffix)
		{
		    var result = new List<Task>();
			for (int cnt = 0; cnt < numTasks; cnt++)
			{
                result.Add(new Task() { Id = Guid.NewGuid(), Text = "Task" + (cnt + 1) + suffix });
			}
		    return result;
		}

		private IList<bool> CreateTestWipLimitViolations(int numColumns)
		{
			var result = new List<bool>();
			for (int cnt = 0; cnt < numColumns; cnt++)
			{
				result.Add(false);
			}
			return result;
		}
	}
}
