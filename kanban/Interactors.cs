﻿using System;
using System.Collections.Generic;
using System.Linq;
using kanban.contracts;
using kanban.logic;
using kanban.resources;

namespace kanban
{
    public class Interactors
    {
        private IKanban _kanban;
        private readonly State<IList<IList<Task>>> _state = new State<IList<IList<Task>>>(); 
        private readonly State<IList<int>> _wipLimitState = new State<IList<int>>() ;

        public void Start(out IEnumerable<ColumnHeader> columnHeaders, out IList<IList<Task>> columnsOfTasks, out IList<bool> wipLimitViolations)
        {
            _kanban = new Kanban();
            var boardConfigProvider = new BoardConfigProvider();
            var taskFileProvider = new TaskFileProvider();
            var kanbanWipLimits = new KanbanWipLimits();

            var boardConfig = boardConfigProvider.ReadConfig();
            columnHeaders = _kanban.CreateColumnHeaders(boardConfig);

            var taskfileLines = taskFileProvider.ReadTaskFile();
            columnsOfTasks = _kanban.CreateTasks(taskfileLines);
			
            var wipLimits = KanbanWipLimits.CreateWipLimits(columnHeaders);
            _wipLimitState.Set(wipLimits);
            wipLimitViolations = kanbanWipLimits.CalculateWipLimitViolations(columnsOfTasks, wipLimits);
            _state.Set(columnsOfTasks);
        }

        public IList<IList<Task>> New(string text)
        {
            var columnsOfTasks = _state.Get().ToList();
            var task = _kanban.CreateTask(text);
            columnsOfTasks = _kanban.AppendTask(task, columnsOfTasks).ToList() ;
            _state.Set(columnsOfTasks);
            Save(columnsOfTasks);

            return columnsOfTasks;
        }

        public IList<IList<Task>> Select(Guid selected)
        {
            _kanban.DeselectAll(_state.Get());
            var task = _kanban.FindTaskById(selected, _state.Get());
            _kanban.SelectTask(task);

            return _state.Get();
        }

        public IList<IList<Task>> Modify(string text)
        {
            var columnsOfTasks = _state.Get().ToList();
            var task = _kanban.FindSelectedTask(columnsOfTasks);
            _kanban.SetText(task, text);
            Save(columnsOfTasks);
            return columnsOfTasks;
        }

        public void Move(Direction direction, out IList<IList<Task>> columnsOfTasks, out IList<bool> limitsExcedded)
        {
            var kanabanWipLimits = new KanbanWipLimits();

            columnsOfTasks = _state.Get();
            var task = _kanban.FindSelectedTask(columnsOfTasks);
            var sourceCoordinate =_kanban.GetSourceCoordinate(task, columnsOfTasks);
            var destinationCoordinate= _kanban.CalculateDestinationCoordinate(sourceCoordinate, direction);
            columnsOfTasks = _kanban.MoveTask(columnsOfTasks, sourceCoordinate, destinationCoordinate);
            Save(columnsOfTasks);
            var wipLimits = _wipLimitState.Get();
            limitsExcedded = kanabanWipLimits.CalculateWipLimitViolations(columnsOfTasks, wipLimits);
        }

        private void Save(IList<IList<Task>> columnsOfTasks)
        {
            var taskFileProvider = new TaskFileProvider();
            IEnumerable<string> lines = _kanban.SerializeTasks(columnsOfTasks);

            taskFileProvider.WriteTaskFile(lines);
        }

    }
}