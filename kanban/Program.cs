﻿using System;
using System.Windows;
using System.Collections.Generic;
using kanban.contracts;
using kanban.ui;

namespace kanban
{
    public class Program
    {
        [STAThread]
        public static void Main()
        {
            var interactors = new Interactors();
			IEnumerable<ColumnHeader> columnHeaders;
			IList<IList<Task>> columnsOfTasks;
			IList<bool> wipLimitViolations;
			interactors.Start(out columnHeaders, out columnsOfTasks, out wipLimitViolations);

            var mainWindow = new MainWindow();
			mainWindow.Init(columnHeaders, columnsOfTasks, wipLimitViolations);

            mainWindow.OnNew = interactors.New;
            mainWindow.OnSelect = interactors.Select;
            mainWindow.OnModify = interactors.Modify;
			mainWindow.OnMove = interactors.Move;
            var shell = new MainShell(mainWindow);
			var app = new Application();
            app.MainWindow = shell;

            app.Run(shell);
        }
    }
}