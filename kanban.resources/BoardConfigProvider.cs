﻿using System.Collections.Generic;
using System.IO;

namespace kanban.resources
{
    public class BoardConfigProvider: kanban.contracts.IBoardConfigProvider
    {
        private string _configFileName = @"InputData\board.config";
        public IEnumerable<string> ReadConfig()
        {
            return File.ReadAllLines(_configFileName);
        }
    }
}
