﻿using System.Collections.Generic;
using System.IO;
using kanban.contracts;

namespace kanban.resources
{
    public class TaskFileProvider : ITaskFileProvider
    {
        private const string _inputdataTasksTxt = "InputData\\tasks.txt";

        public IEnumerable<string> ReadTaskFile()
        {
             return File.ReadAllLines(_inputdataTasksTxt);
        }

        public void WriteTaskFile(IEnumerable<string> lines)
        {
            File.WriteAllLines(_inputdataTasksTxt, lines);
        }
    }
}
