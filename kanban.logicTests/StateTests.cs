﻿using System.Collections.Generic;
using kanban.logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace kanban.logic.Tests
{
    [TestClass]
    public class StateTests
    {
        [TestMethod]
        public void TestStateAsInt()
        {
            var state = new State<int>();
            int testInt = 5;

            Assert.AreNotEqual(state.Get(), testInt);

            state.Set(testInt);

            Assert.AreEqual(state.Get(), testInt);
        }

        [TestMethod]
        public void TestStateAsIEnumerable()
        {
            var state = new State<IEnumerable<IEnumerable<int>>>();
            int testInt = 5;
            var testData = new List<List<int>>();

            var col1 = new List<int>();
            var col2 = new List<int>();

            col1.Add(5);
            col1.Add(15);
            testData.Add(col1);
            testData.Add(col2);

            Assert.AreNotEqual(state.Get(), testInt);

            state.Set(testData);

            Assert.AreEqual(state.Get(), testData);
        }
    }
}
