﻿using System;
using System.Collections.Generic;
using System.Linq;
using kanban.contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace kanban.logic.Tests
{
    [TestClass]
    public class KanbanWipLimitsTest
    {
        private KanbanWipLimits _sut;

        public KanbanWipLimitsTest()
        {
            _sut = new KanbanWipLimits();
        }

        [TestMethod]
        public void CreateTasksTestOnEmptyList()
        {

            var result = _sut.CalculateWipLimitViolations(new List<IList<Task>>(), new List<int>());

            Assert.AreEqual(0, result.Count);

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CreateTasksTestOnDifferentListCount()
        {
            var result = _sut.CalculateWipLimitViolations(new List<IList<Task>>(), new List<int>() { 1 });
            Assert.Fail("Exception expected");
        }


        [TestMethod]
        public void CreateTasksTestOnFilledList()
        {

            var columnsOfTasks = GetColumnsOfTasksWith3x3();
            var result = _sut.CalculateWipLimitViolations(columnsOfTasks, new List<int>() { 1, 1, 5 });

            Assert.AreEqual(3, result.Count);

            Assert.IsTrue(result[0], "Result index 0 wrong");
            Assert.IsTrue(result[1], "Result index 1 wrong");
            Assert.IsFalse(result[2], "Result index 2 wrong");

        }
        [TestMethod]
        public void CreateTasksTestOnFilledListTestZeros()
        {

            var columnsOfTasks = GetColumnsOfTasksWith3x3();
            var result = _sut.CalculateWipLimitViolations(columnsOfTasks, new List<int>() { 0, 0, 0 });

            Assert.AreEqual(3, result.Count);

            Assert.IsFalse(result[0], "Result index 0 wrong");
            Assert.IsFalse(result[1], "Result index 1 wrong");
            Assert.IsFalse(result[2], "Result index 2 wrong");

        }

        [TestMethod]
        public void CreateTasksTestOnFilledListTestSomeZeros()
        {

            var columnsOfTasks = GetColumnsOfTasksWith3x3();
            var result = _sut.CalculateWipLimitViolations(columnsOfTasks, new List<int>() { 1, 0, 0 });

            Assert.AreEqual(3, result.Count);

            Assert.IsTrue(result[0], "Result index 0 wrong");
            Assert.IsFalse(result[1], "Result index 1 wrong");
            Assert.IsFalse(result[2], "Result index 2 wrong");

        }

        private IList<IList<Task>> GetColumnsOfTasksWith3x3()
        {
            var columnsOfTasks = new List<IList<Task>>
            {
                new List<Task>
                {
                    new Task {},
                    new Task {},
                    new Task {}
                },
                new List<Task>
                {
                   new Task {},
                   new Task {},
                   new Task {},
                   },
                new List<Task>
                {
                  new Task {},
                  new Task {},
                  new Task {},
                  }
            };

            return columnsOfTasks;
        }
    }
}
