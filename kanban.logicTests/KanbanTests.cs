﻿using System;
using System.Collections.Generic;
using System.Linq;
using kanban.contracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace kanban.logic.Tests
{
    [TestClass]
    public class KanbanTests
    {
        private Kanban _sut;

        public KanbanTests()
        {
            _sut = new Kanban();
        }
        [TestMethod]
        public void CreateColumnHeadersTest()
        {
            string[] testdata = { "ready", "doing;3", "done" };
            var result = _sut.CreateColumnHeaders(testdata);

            var list = result.ToList();

            Assert.AreEqual(list[0].Name, "ready");
            Assert.AreEqual(list[0].WipLimit, 0);
            Assert.AreEqual(list[1].Name, "doing");
            Assert.AreEqual(list[1].WipLimit, 3);
            Assert.AreEqual(list[2].Name, "done");
            Assert.AreEqual(list[2].WipLimit, 0);
        }

        [TestMethod]
        public void CreateTasksTest()
        {
            string[] testdata = { "task1,task2,task3", "taskDoing1,taskDoing2", "" };
            var result = _sut.CreateTasks(testdata);

            var column = result.ToList();
            var col1 = column[0].ToArray();
            Assert.AreEqual(col1[0].Text, "task1");
            Assert.AreEqual(col1[1].Text, "task2");
            Assert.AreEqual(col1[2].Text, "task3");

            var col2 = column[1].ToArray();
            Assert.AreEqual(col2[0].Text, "taskDoing1");
            Assert.AreEqual(col2[1].Text, "taskDoing2");

            Assert.IsFalse(column[2].Any());
        }

        [TestMethod]
        public void AppendTaskTest()
        {
            Task testTask = new Task();
            testTask.Text = "myText";
            testTask.Id = Guid.NewGuid();

            var columnsOfTasks = new List<IList<Task>> { new List<Task>(), new List<Task>() };

            var result = _sut.AppendTask(testTask, columnsOfTasks);

            Assert.AreEqual(result[0].ToArray()[0], testTask);
        }

        [TestMethod]
        public void DeselectAllTest()
        {
            var columnsOfTasks = new List<IList<Task>>
            {
                new List<Task>
                {
                    new Task {Selected = true},
                    new Task {Selected = true},
                    new Task {Selected = true}
                },
                new List<Task>
                {
                    new Task {Selected = true},
                    new Task {Selected = true},
                    new Task {Selected = true}
                },
                new List<Task>
                {
                    new Task {Selected = true},
                    new Task {Selected = true},
                    new Task {Selected = true}
                }
            };

            _sut.DeselectAll(columnsOfTasks);

            foreach (var column in columnsOfTasks)
            {
                foreach (var task in column)
                {
                    Assert.IsFalse(task.Selected);
                }
            }
        }

        [TestMethod]
        public void FindSelectedTaskTest()
        {
            Guid id = Guid.NewGuid();
            var columnsOfTasks = new List<IList<Task>>
            {
                new List<Task>
                {
                    new Task {Selected = false},
                    new Task {Selected = false},
                    new Task {Selected = false}
                },
                new List<Task>
                {
                    new Task {Selected = false},
                    new Task {Id = id, Selected = true},
                    new Task {Selected = false}
                },
                new List<Task>
                {
                    new Task {Selected = false},
                    new Task {Selected = false},
                    new Task {Selected = false}
                }
            };

            var result = _sut.FindSelectedTask(columnsOfTasks);

            Assert.AreEqual(result.Id, id);
        }


        [TestMethod]
        public void SetTextTest()
        {

            var expected = "dies ist der neue Text";
            var textToSet = expected;
          
            var task = new Task();
            task.Text = "initial Text";

            _sut.SetText(task, textToSet);

            Assert.AreEqual(expected, task.Text);
        }

        [TestMethod]
        public void SelectTaskTest()
        {
            var task = new Task();

            _sut.SelectTask(task);

            Assert.IsTrue(task.Selected);
        }



        [TestMethod]
        public void FindTaskByIdTest()
        {
            var expectedGuid = Guid.NewGuid();
            var expectedTask = new Task {Id = expectedGuid};
            var columnsOfTasks = new List<IList<Task>>
            {
                new List<Task>
                {
                    new Task {Id = Guid.NewGuid()},
                    new Task {Id = Guid.NewGuid()},
                    new Task {Id = Guid.NewGuid()}
                },
                new List<Task>
                {
                    new Task {Id = Guid.NewGuid()},
                    expectedTask,
                    new Task {Id = Guid.NewGuid()}
                },
                new List<Task>
                {
                    new Task {Id = Guid.NewGuid()},
                    new Task {Id = Guid.NewGuid()},
                    new Task {Id = Guid.NewGuid()}
                }
            };

            var result = _sut.FindTaskById(expectedGuid, columnsOfTasks);

            Assert.AreSame(expectedTask, result);
        }
    }
}