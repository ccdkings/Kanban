﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using kanban.contracts;
using Task = kanban.contracts.Task;

namespace kanban.logic
{
    public class Kanban : IKanban
    {
        public IEnumerable<ColumnHeader> CreateColumnHeaders(IEnumerable<string> configLines)
        {
            IList<ColumnHeader> columnHeaders = new List<ColumnHeader>();
            foreach (string configLine in configLines)
            {
                var columnHeader = new ColumnHeader();
                var values = configLine.Split(';');
                columnHeader.Name = values[0];
                columnHeader.WipLimit = values.Length == 1 ? 0: int.Parse(values[1]);
                columnHeaders.Add(columnHeader);
            }
            return columnHeaders;
        }

        public IList<IList<Task>> CreateTasks(IEnumerable<string> taskLines)
        {
            var ListTask = new List<IList<Task>>();

            foreach (var taskLine in taskLines)
            {
                var texts = taskLine.Split(',');
                var column = new List<Task>();
                foreach (string text in texts)
                {
                    if(string.IsNullOrEmpty(text))
                        continue;
                    
                    column.Add(CreateTask(text));
                }
                ListTask.Add(column);
            }

            return ListTask;
        }

        public Task CreateTask(string text)
        {
            var task = new Task()
            {
                Text = text,
                Id = Guid.NewGuid()
            };
            return task;
        }

        public IList<IList<Task>> AppendTask(Task task, IList<IList<Task>> columnsOfTasks)
        {
            var firstColumn = columnsOfTasks[0];
            columnsOfTasks[0] = AppendTaskToColumn(firstColumn, task);
            return columnsOfTasks;
        }

        private IList<Task> AppendTaskToColumn(IList<Task> column, Task task)
        {
            var result = new List<Task>(column);
            result.Add(task);
            return result;
        }

        public IEnumerable<string> SerializeTasks(IList<IList<Task>> columnsOfTasks)
        {
            foreach (var column in columnsOfTasks)
            {
                StringBuilder line = new StringBuilder();

                foreach (var task in column)
                {
                    line.Append(task.Text);
                    line.Append(",");
                }
                string newLine = line.ToString();
                newLine = newLine.TrimEnd(',');

                yield return newLine;
            }
        }

        public void DeselectAll(IList<IList<Task>> columnsOfTasks)
        {
            foreach (var columns in columnsOfTasks)
            {
                foreach (var task in columns)
                {
                    task.Selected = false;
                }
            }
        }

        public Task FindTaskById(Guid selected, IList<IList<Task>> columnsOfTasks)
        {
             return (from column in columnsOfTasks from task in column where task.Id == selected select task).First();
        }

        public void SelectTask(Task task)
        {
            task.Selected = true;
        }

        public Task FindSelectedTask(IList<IList<Task>> columnsOfTasks)
        {
            return (from column in columnsOfTasks from task in column where task.Selected select task).First();
        }

        public void SetText(Task task, string text)
        {
            task.Text = text;
        }

        public Tuple<int, int> GetSourceCoordinate(Task task, IList<IList<Task>> columnsOfTasks)
        {
            var column = columnsOfTasks.Single(taskColumn => taskColumn.IndexOf(task) >= 0);
            return new Tuple<int, int>(columnsOfTasks.IndexOf(column), column.IndexOf(task));
        }

        public Tuple<int, int> CalculateDestinationCoordinate(Tuple<int, int> source, Direction direction)
        {
            int column = source.Item1;
            int row = source.Item2;
            switch (direction)
            {
                case Direction.Up:
                    row--;
                    break;
                case Direction.Down:
                    row++;
                    break;
                case Direction.Left:
                    column--;
                    break;
                case Direction.Right:
                    column++;
                    break;
            }
            return new Tuple<int, int>(column,row);
        }

        public IList<IList<Task>> MoveTask(IList<IList<Task>> columnsOfTasks, Tuple<int, int> source,
            Tuple<int, int> destination)
        {
            if (!(destination.Item1 < 0
                || destination.Item2 < 0
                || destination.Item1 >= columnsOfTasks.Count))                
            {

                var task = columnsOfTasks[source.Item1][source.Item2];
                columnsOfTasks[source.Item1].RemoveAt(source.Item2);
                var targetRow = destination.Item2 >= columnsOfTasks[destination.Item1].Count ? columnsOfTasks[destination.Item1].Count : destination.Item2;
                columnsOfTasks[destination.Item1].Insert(targetRow, task);
            }
            return columnsOfTasks;
        }
    }
}
