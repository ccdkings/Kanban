﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using kanban.contracts;


namespace kanban.logic
{
    public class KanbanWipLimits
    {
        public IList<bool> CalculateWipLimitViolations(IList<IList<Task>> columnsOfTasks, IList<int> wipLimits)
        {
            if (columnsOfTasks.Count != wipLimits.Count)
            {
                throw new ArgumentException("columnsOfTasks.Count != wipLimits.Count");
            }

            var wipLimitViolations = columnsOfTasks.Zip(wipLimits,
                (columnOfTasks, wipLimit) => wipLimit != 0 && columnOfTasks.Count > wipLimit);

            return wipLimitViolations.ToList();
        }

        public static List<int> CreateWipLimits(IEnumerable<ColumnHeader> columnHeaders)
        {
            return columnHeaders.Select(x => x.WipLimit).ToList();
        }
    }
}
