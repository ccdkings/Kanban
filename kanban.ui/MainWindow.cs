﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using kanban.contracts;
using kanban.ui.wpf;

namespace kanban.ui
{
	public class MainWindow : ViewModelBase, IMainWindow
	{
		public MainWindow()
		{
			NewCommand = new RelayCommand(DoNew);
			ModifyCommand = new RelayCommand(DoModify);
			DeleteCommand = new RelayCommand(DoDelete);
			UpCommand = new RelayCommand(DoUp);
			LeftCommand = new RelayCommand(DoLeft);
			RightCommand = new RelayCommand(DoRight);
			DownCommand = new RelayCommand(DoDown);
		}

		public void Init(IEnumerable<ColumnHeader> columnHeaders, IList<IList<Task>> columnsOfTasks, IList<bool> wipLimitExceeded)
		{
			if (columnHeaders == null)
			{
				throw new ArgumentNullException("columnHeaders");
			}

			if (columnsOfTasks == null)
			{
				throw new ArgumentNullException("columnsOfTasks");
			}

			if (wipLimitExceeded == null)
			{
				throw new ArgumentNullException("wipLimitExceeded");
			}

			if ((columnHeaders.Count() != columnsOfTasks.Count()) || (columnHeaders.Count() != wipLimitExceeded.Count()))
			{
				throw new ArgumentOutOfRangeException("columnHeaders.Count != columnsOfTasks.Count != wipLimitExceeded.Count");
			}

			var columnList = Enumerable.Range(0, columnHeaders.Count())
				.Select(n => new ColumnVM(columnHeaders.ElementAt(n), columnsOfTasks.ElementAt(n), wipLimitExceeded.ElementAt(n), DoSelect)).ToList();
			Columns = new ObservableCollection<ColumnVM>(columnList);
		}

	    public Func<string, IList<IList<Task>>> OnNew { get; set; }
	    public Func<Guid, IList<IList<Task>>> OnSelect { get; set; }
		public Func<string, IList<IList<Task>>> OnModify { get; set; }
		public OnMoveAction OnMove { get; set; }

	    public ObservableCollection<ColumnVM> Columns
		{
			get { return Get<ObservableCollection<ColumnVM>>(); }
			private set { Set(value); }
		}

		public string Text
		{
			get { return Get<string>(); }
			set { Set(value); }
		}

		public ICommand NewCommand { get; private set; }
		public ICommand ModifyCommand { get; private set; }
		public ICommand DeleteCommand { get; private set; }
		public ICommand UpCommand { get; private set; }
		public ICommand LeftCommand { get; private set; }
		public ICommand RightCommand { get; private set; }
		public ICommand DownCommand { get; private set; }

		private void DoNew()
		{
			var columnsOfTasks = OnNew(Text);
			RefreshUI(columnsOfTasks);
		}

		private void DoSelect(Guid id)
		{
			var columnsOfTasks = OnSelect(id);
			RefreshUI(columnsOfTasks);
		}

		private void DoModify()
		{
			var columnsOfTasks = OnModify(Text);
			RefreshUI(columnsOfTasks);
		}

		private void DoDelete()
		{
			throw new NotImplementedException();
		}

		private void DoUp()
		{
			DoMove(Direction.Up);
		}

		private void DoLeft()
		{
			DoMove(Direction.Left);
		}

		private void DoRight()
		{
			DoMove(Direction.Right);
		}

		private void DoDown()
		{
			DoMove(Direction.Down);
		}

		private void DoMove(Direction direction)
		{
			IList<IList<Task>> columnsOfTasks;
			IList<bool> wipLimitExceeded;

			OnMove(direction, out columnsOfTasks, out wipLimitExceeded);
			RefreshUI(columnsOfTasks);
		}

		private void RefreshUI(IList<IList<Task>> columnsOfTasks)
		{
			RefreshColumns(columnsOfTasks);
			RefreshText(columnsOfTasks);
		}

		private void RefreshColumns(IList<IList<Task>> columnsOfTasks)
		{
			if (Columns.Count() != columnsOfTasks.Count())
			{
				throw new ArgumentOutOfRangeException("Columns.Count != columnsOfTasks.Count");
			}

			Enumerable.Range(0, Columns.Count()).ToList().ForEach(n => Columns[n].RefreshTasks(columnsOfTasks.ElementAt(n)));
		}

		private void RefreshText(IList<IList<Task>> columnsOfTasks)
		{
			var query = columnsOfTasks.SelectMany(list => list.Where(task => task.Selected == true));
			if (query.Any())
			{
				Text = query.First().Text;
			}
			else
			{
				Text = "";
			}
		}
	}
}
