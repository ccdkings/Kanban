﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using kanban.contracts;
using kanban.ui.wpf;

namespace kanban.ui
{
	public class ColumnVM : ViewModelBase
	{
		ColumnHeader header;

		public ColumnVM(ColumnHeader header, IEnumerable<Task> tasks, bool wipLimitViolation, Action<Guid> onSelect)
		{
			if (header == null)
			{
				throw new ArgumentNullException("header");
			}

			if (tasks == null)
			{
				throw new ArgumentNullException("tasks");
			}

			if (onSelect == null)
			{
				throw new ArgumentNullException("onSelect");
			}

			this.header = header;
			WipLimitExceeded = wipLimitViolation;
			OnSelect = onSelect;

			BuildTaskCollection(tasks);
		}

		public string Title
		{
			get
			{
				if (header.WipLimit > 0)
				{
					return string.Format("{0} ({1})", header.Name, header.WipLimit);
				}
				return header.Name;
			}
		}

	    public ObservableCollection<TaskVM> Tasks
	    {
            get { return Get<ObservableCollection<TaskVM>>(); }
            private set { Set(value); }
        }

		public bool WipLimitExceeded
		{
			get { return Get<bool>(); }
			private set { Set(value); }
		}

        public void RefreshTasks(IEnumerable<Task> tasks)
		{
			BuildTaskCollection(tasks);
		}

		private Action<Guid> OnSelect { get; set; }

		private void BuildTaskCollection(IEnumerable<Task> tasks)
		{
			Tasks = new ObservableCollection<TaskVM>();
			foreach (var task in tasks)
			{
				Tasks.Add(new TaskVM(task, OnSelect));
			}
		}
	}
}
