﻿using System;
using System.Windows.Input;
using kanban.contracts;
using kanban.ui.wpf;

namespace kanban.ui
{
	public class TaskVM
	{
		Task task;

		public TaskVM(Task task, Action<Guid> onSelect)
		{
			if (task == null)
			{
				throw new ArgumentNullException("task");
			}

			if (onSelect == null)
			{
				throw new ArgumentNullException("onSelect");
			}

			SelectCommand = new RelayCommand(DoSelect);

			this.task = task;
			OnSelect = onSelect;
		}

		public ICommand SelectCommand { get; private set; }

		public Guid Id { get { return task.Id; } }
		public string Text { get { return task.Text; } }
		public bool Selected { get { return task.Selected; } }

		private Action<Guid> OnSelect { get; set; }

		private void DoSelect()
		{
			OnSelect(Id);
		}
	}
}
