﻿using System;
using System.Windows.Input;

namespace kanban.ui.wpf
{
	/// <summary>
	/// A command whose sole purpose is to relay its functionality to other objects by invoking delegates.
	/// The default return value for the CanExecute method is 'true'.
	/// </summary>
	public class RelayCommand : ICommand
	{
		readonly Func<Boolean> _canExecute;
		readonly Action _execute;
		EventHandler _internalCanExecuteChanged;

		public RelayCommand(Action execute)
			: this(execute, null)
		{ }

		public RelayCommand(Action execute, Func<Boolean> canExecute)
		{
			if (execute == null)
			{
				throw new ArgumentNullException("execute");
			}
			_execute = execute;
			_canExecute = canExecute;
		}

		public void RaiseCanExecuteChanged()
		{
			if (_canExecute != null)
			{
				OnCanExecuteChanged();
			}
		}

		protected virtual void OnCanExecuteChanged()
		{
			EventHandler canExecuteChanged = _internalCanExecuteChanged;
			if (canExecuteChanged != null)
			{
				canExecuteChanged(this, EventArgs.Empty);
			}
		}

		public event EventHandler CanExecuteChanged
		{
			add
			{
				if (_canExecute != null)
				{
					_internalCanExecuteChanged += value;
				}
			}
			remove
			{
				if (_canExecute != null)
				{
					_internalCanExecuteChanged -= value;
				}
			}
		}

		public Boolean CanExecute(Object parameter)
		{
			return _canExecute == null ? true : _canExecute();
		}

		public void Execute(Object parameter)
		{
			_execute();
		}
	}
}
