﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace kanban.ui.wpf
{
	public class ViewModelBase : INotifyPropertyChanged
	{
		Dictionary<string, object> properties = new Dictionary<string, object>();

		protected T Get<T>([CallerMemberName] string name = null)
		{
			object value = null;
			if (properties.TryGetValue(name, out value))
			{
				return value == null ? default(T) : (T)value;
			}
			return default(T);
		}

		protected bool Set<T>(T value, [CallerMemberName] string name = null)
		{
			if (Equals(value, Get<T>(name)))
			{
				return false;
			}
			properties[name] = value;
			OnPropertyChanged(name);
			return true;
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}
