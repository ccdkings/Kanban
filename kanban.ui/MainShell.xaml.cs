﻿using System.Collections.Generic;
using kanban.contracts;

namespace kanban.ui
{
	/// <summary>
	/// Interaction logic for MainShell.xaml
	/// </summary>
	public partial class MainShell
    {
		public MainShell(IMainWindow vm)
		{
			InitializeComponent();

			DataContext = vm;
		}
    }
}
