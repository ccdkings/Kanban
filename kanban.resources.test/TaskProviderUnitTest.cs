﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace kanban.resources.test
{
    [TestClass]
    public class TaksProviderUnitTest
    {
        [TestMethod]
        public void ReadTaskFileTest()
        {
            var taskFileProvider = new TaskFileProvider();
            var tasksStrings = taskFileProvider.ReadTaskFile();

            Assert.IsTrue(tasksStrings.ToArray().Length > 0);
        }


        [TestMethod]
        public void WriteTaskFileTest()
        {
            var lines = new string[] {"task1,task2,task3", "taskDoing1,taskDoing2", "taskDone1,taskDone2"};
            var taskFileProvider = new TaskFileProvider();

            taskFileProvider.WriteTaskFile(lines);
        }


        [TestMethod]
        public void WriteTaskFileAndReadTest()
        {
            var taskFileProvider = new TaskFileProvider();

            // save in order to reset later
            var saveStrings = taskFileProvider.ReadTaskFile();
            var enumerable = saveStrings as IList<string> ?? saveStrings.ToList();
            var saveCount = enumerable.ToArray().Length;

            var lines = new string[] { "task1,task2,task3", "taskDoing1,taskDoing2", "taskDone1,taskDone2", "taskRemaining" };
            taskFileProvider.WriteTaskFile(lines);
            var tasksStrings = taskFileProvider.ReadTaskFile();

            var taskArray = tasksStrings as string[] ?? tasksStrings.ToArray();
            Assert.IsTrue(taskArray.Length == 4);
            Assert.IsTrue(taskArray[3] == "taskRemaining");

            // try to restore
            taskFileProvider.WriteTaskFile(enumerable);
            Assert.IsTrue(taskFileProvider.ReadTaskFile().ToArray().Length == saveCount);
        }
    }
}
