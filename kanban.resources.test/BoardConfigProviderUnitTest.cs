﻿using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using kanban.resources;
namespace kanban.resources.test
{
    [TestClass]
    public class BoardConfigProviderUnitTest
    {
        [TestMethod]
        public void ReadConfigTest()
        {
            var target = new BoardConfigProvider();
            var result = target.ReadConfig();

            Assert.IsNotNull(result);
            var enumerable = result as string[] ?? result.ToArray();
            Assert.IsTrue(  enumerable.Count() == 3, "Wrong number auf lines read");
            Assert.IsTrue(enumerable.FirstOrDefault() == "ready", "Value is not 'ready'");
            Assert.IsTrue(enumerable.Contains("doing;3"), "Value is not 'doing;3'");
            Assert.IsTrue(enumerable.Contains("done"), "Value is not 'doing;3'");
            Assert.IsFalse(enumerable.Contains("not existing"), "Value is not 'doing;3'");
            Assert.IsTrue(enumerable.FirstOrDefault() == "ready", "Value is not 'ready'");

        }
    }
}
